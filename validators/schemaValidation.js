const Joi = require('@hapi/joi');

const idSchema = Joi.number().integer();

const mangaSchema = Joi.object({
    mangaId: Joi.number()
        .integer()
        .required(),

    name: Joi.string()
        .min(5)
        .max(30)
        .required(),

    desc: Joi.string()
        .min(0),

    category: Joi.array()
        .min(2)
});

const updateMangaSchema = Joi.object({
    name: Joi.string()
        .min(5)
        .max(30)
        .required(),

    desc: Joi.string()
        .min(0),

    category: Joi.array()
        .min(2)
});

const categorySchema = Joi.object({
    category: Joi.string()
        .max(15)
        .required()
});

const userSchema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/),

    repeat_password: Joi.ref('password'),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'io'] } })
});

function idValidation(req, res, next) {
    let id = req.params.id;

    const { error, value } = idSchema.validate(id);

    if (error) {
        next({ Error: 'Not Acceptable', error });
    } else {
        next();
    }
};

function postMangaValidation(req, res, next) {
    let newManga = req.body;

    // console.log(newManga);
    const { err, value } = mangaSchema.validate(newManga);

    if (err) {
        next({ Error: 'Bad Request', msg: 'Please input the manga name and id in a correct format.', err });
    } else {
        req.manga = value;
        // console.log(req.manga);
        next();
    }
}

function updateMangaValidation(req, res, next) {
    let updateData = req.body;

    const { err, value } = updateMangaSchema.validate(updateData);

    if (err) {
        next({ Error: 'Bad Request', msg: 'Please input the updated manga data in a correct format.', err });
    } else {
        req.updateManga = value;
        next();
    }
}

function postCategoryValidation(req, res, next) {
    let postCategory = req.body;

    const { error, value } = categorySchema.validate(postCategory);

    if (error) {
        next({ Error: 'Bad Request', msg: 'Please input the category in a correct format.', err });
    } else {
        req.newCategory = value;
        next();
    }
}

function registerUserValidation(req, res, next) {
    let newUser = req.body;

    const { error, value } = userSchema.validate(newUser);

    if (error) {
        next('Please input the userName email and password in the correct format');
    } else {
        next();
    }
}

module.exports = {
    idValidation,
    postMangaValidation,
    updateMangaValidation,
    postCategoryValidation,
    registerUserValidation
};