let query = require('./queries');

async function addIntoMangaCategory(genre) {
    let categoryList = await query.Queries(query.allQuery.allCategoryQuery);
    let arr = genre.map(element => categoryList.filter(cat => cat.category === element)[0].id);

    return arr;
}

async function modifyingArrayForInserting(genre, mangaId) {
    let mangaCategory = await addIntoMangaCategory(genre);

    let newMangaCategory = [];
    mangaCategory.forEach(categoryId => {
        let newMC = [];
        newMC.push(parseInt(mangaId, 10));
        newMC.push(categoryId);
        newMangaCategory.push(newMC);
    });

    return newMangaCategory;
}

async function allCategoryId() {
    try {
        let categoryId = await query.Queries(query.allQuery.allCategoryQuery);

        let categoryIdArray = categoryId.map(e => e.id);

        return categoryIdArray;

    } catch (error) {
        throw error;
    }

}

module.exports = {
    modifyingArrayForInserting,
    addIntoMangaCategory,
    // allCategoryId
};