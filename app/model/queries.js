const connection = require('./connection');

function connected() {
    connection.connect((err) => {
        if (err) {
            throw err;
        }
        console.log('MySql Connected...');
    });
}

function Queries(query, data = []) {
    return new Promise((resolve, reject) => {
        connection.query(query, [data], (err, results) => {
            if (err) reject(err);

            resolve(results);
        });
    });
}

function otherQueries(query, data = []) {
    return new Promise((resolve, reject) => {
        connection.query(query, [data], (err, results) => {
            if (err) throw err;

            // console.log(results);
            resolve(results);
        });
    });
}

let allQuery = {
    allMangaQuery: `select * from Manga`,
    singleMangaQuery: `select * from Manga where mangaId = ?`,
    addingMangaQuery: `insert into Manga set ?`,
    allCategoryQuery: `select * from Category;`,
    addingMangaCategoryQuery: `insert into MangaCategory (mangaId, categoryId) values ?`,
    updateMangaQuery: 'update Manga set name = ?, description = ? where mangaId = ?',
    deleteMangaCategoryQuery: 'delete from MangaCategory where mangaId = ?',
    deleteMangaQuery: 'delete from Manga where mangaId = ?',
    singleCategoryQuery: `select * from Category where id = ?`,
    addingCategoryQuery: `insert into Category set ?`,
    mangaOfSingleCategory: `select M.*, C.* from Manga as M
                            left join MangaCategory as MC on M.mangaId = MC.mangaId
                            left join Category as C on MC.categoryId = C.id
                            where C.id = ?`,
    categoryOfSingleManga: `select M.*, C.* from Manga as M
                            left join MangaCategory as MC on M.mangaId = MC.mangaId
                            left join Category as C on MC.categoryId = C.id
                            where M.mangaId = ?`,
    addingUser: `insert into user set ?`,
    findUserByEmail: `select * from user where email = ?`
};

module.exports = {
    connected,
    allQuery,
    Queries,
    otherQueries
};