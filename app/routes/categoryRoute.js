const express = require("express");
const router = express.Router();
const queries = require('../model/queries');
const validation = require('../../validators/schemaValidation');
const verify = require('../../middleware/verifyToken');

/*==========================
 Get all Category
 =========================*/

router.get('/category', (req, res, next) => {
    queries.Queries(queries.allQuery.allCategoryQuery).then((results) => {
        if (req.headers['content-type'] === 'application/json') {
            res.json(results);
            next();
        }
    }).catch((err) => {
        next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' });
    });
});

/*=========================
Get Single Category
=========================*/

router.get('/category/:id', validation.idValidation, (req, res, next) => {
    queries.Queries(queries.allQuery.singleCategoryQuery, req.params.id)
        .then((result) => {
            if (result.length === 0) {
                next({ Error: 'Request Not Found', msg: `Manga with manga id: ${req.params.id} doesn't exist.` });
            }
            if (req.headers['content-type'] === 'application/json') {
                res.json(result);
                next();
            }
        }).catch((err) => {
            next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' });
        });
});

/*=======================
Adding New Category
=======================*/

router.post('/category',
    verify.verifyToken,
    validation.postCategoryValidation,
    (req, res) => {
        const newCategory = req.body.category;
        const validatedCategory = req.newCategory;
        const category = {
            category: newCategory
        };

        queries.Queries(queries.allQuery.addingCategoryQuery, category).then((result) => {
            res.send({ validatedCategory, msg: "Successfully added new category." });
        }).catch((err) => {
            res.status(404).send({ err, msg: `Category already exists` });
        });
    });

module.exports = router;