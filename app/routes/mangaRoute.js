const express = require("express");
const router = express.Router();
const queries = require('../model/queries');
const model = require('../model/dataManipulation');
const validation = require('../../validators/schemaValidation');
const verify = require('../../middleware/verifyToken');

/*======================
Get All Manga
======================*/

router.get('/manga', (req, res, next) => {

    queries.Queries(queries.allQuery.allMangaQuery).then((results) => {
        if (req.headers['content-type'] === 'application/json') {
            res.json(results);
        } else {
            res.render('mangaList', { title: 'Manga List', results });
        }
    }).catch((err) => {
        next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' });
    });
});

/*=========================
GET Single manga
=========================*/

router.get("/manga/:id", validation.idValidation, (req, res, next) => {

    queries.Queries(queries.allQuery.singleMangaQuery, req.params.id).then((result) => {
        if (result.length === 0) {
            next({ Error: 'Request Not Found', msg: "Manga doesn't exist." });
        } else if (req.headers['content-type'] === 'application/json') {
            res.json(result);
            next();
        }
    }).catch((err) => {
        next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' })
    });

});

/*=======================
Posting a manga
=======================*/

router.post('/manga', verify.verifyToken, validation.postMangaValidation, (req, res) => {
    const newManga = {
        genre: req.body.category
    };
    const addManga = {
        mangaId: req.body.mangaId,
        name: req.body.name,
        description: req.body.desc
    };

    const manga = req.manga;

    queries.Queries(queries.allQuery.addingMangaQuery, addManga).then((result) => {
        res.send({ manga, msg: "Successfully added new manga." });
    }).catch((err) => {
        res.send({ err, msg: `Manga already exists` });
    });

    model.modifyingArrayForInserting(newManga.genre, addManga.mangaId)
        .then((newMangaCategory) => {
            console.log(newMangaCategory);
            queries.otherQueries(queries.allQuery.addingMangaCategoryQuery, newMangaCategory);
        });
});

/*=========================
Updating a Manga
=========================*/

router.put('/manga/:id',
    verify.verifyToken,
    validation.idValidation,
    validation.updateMangaValidation,
    (req, res) => {
        let updatedManga = {
            name: req.body.name,
            desc: req.body.desc,
            genre: req.body.category
        };

        const manga = req.updateManga

        queries.Queries(queries.allQuery.updateMangaQuery, [updatedManga.name, updatedManga.desc, req.params.id])
            .then((result) => {
                res.send({ manga, msg: `Successfully updated manga id: ${req.params.id}` });
            }).catch((err) => {
                res.send({ err, msg: "Bad Request: Manga already exists" });
            });

        model.modifyingArrayForInserting(updatedManga.genre, req.params.id)
            .then((result) => {
                queries.otherQueries(queries.allQuery.deleteMangaCategoryQuery, req.params.id);

                queries.otherQueries(queries.allQuery.addingMangaCategoryQuery, result);
            });
    });

/*=====================
Deleting a Manga
=====================*/

router.delete('/manga/:id', validation.idValidation, (req, res) => {

    queries.otherQueries(queries.allQuery.deleteMangaQuery, req.params.id)
        .then((result) => {
            if (result.affectedRows === 1) {
                res.send({ result, msg: "Successfully deleted the Manga." });
            }
            res.status(404).json({ msg: `Bad Request: Either Manga is already deleted or Manga doesn\'t exist.` });
        });

    queries.otherQueries(queries.allQuery.deleteMangaCategoryQuery, req.params.id);
});

module.exports = router;