const express = require("express");
const router = express.Router();
const queries = require('../model/queries');
const validation = require('../../validators/schemaValidation');

/*=================================
GET all Manga of single Category
=================================*/

router.get('/category/manga/:id', validation.idValidation, (req, res, next) => {

    queries.Queries(queries.allQuery.mangaOfSingleCategory, req.params.id)
        .then((results) => {
            if (results.length === 0) {
                next({ Error: 'Request Not Found', msg: "Category doesn't exist." });
            } else if (req.headers['content-type'] === 'application/json') {
                res.json(results);
            } else {
                let categoryName = results[0].category;
                res.render('mangaOfCategory', {
                    title: 'Manga of Category',
                    results,
                    name: categoryName
                });
            }
        }).catch((err) => {
            next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' })
        });
    next();
});

/*==============================
GET Categories of Single Manga
==============================*/

router.get('/manga/category/:id', validation.idValidation, (req, res, next) => {
    queries.Queries(queries.allQuery.categoryOfSingleManga, req.params.id)
        .then((results) => {
            if (results.length === 0) {
                next({ Error: 'Request Not Found', msg: "Manga doesn't exist." });
            } else if (req.headers['content-type'] === 'application/json') {
                res.json(results);
            }
        }).catch((err) => {
            next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' })
        });
    next();
});

module.exports = router;