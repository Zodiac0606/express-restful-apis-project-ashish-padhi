const express = require('express');
const manga = require('./app/routes/mangaRoute');
const category = require('./app/routes/categoryRoute');
const mangaCategory = require('./app/routes/MangaCategoryRoute');
const hbs = require('express-handlebars');
const db = require('./app/model/queries');
const morgan = require('morgan');
const authentication = require('./Authentication/loginRoute');
const errorHandler = require('./middleware/errorHandling');


const app = express();
const port = process.env.PORT || 3000;

db.connected();

app.use(morgan('combined'));

//Setting the handlebars engine
app.engine('handlebars', hbs({ defaultLayout: 'main' }));

app.set('view engine', 'handlebars');

//Body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.listen(port, () => console.log(`App listening on port ${port}!`));

app.use('/user', authentication);
app.use('/user', manga);
app.use('/user', category);
app.use('/user', mangaCategory);
app.use('/user', errorHandler);