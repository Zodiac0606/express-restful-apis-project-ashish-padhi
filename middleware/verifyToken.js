//FORMAT OF TOKEN
// Authorization: Bearer <access_token>
const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];

    // console.log(bearerHeader);

    const secretKey = "animeLover0606";

    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');

        const Token = bearer[1];

        jwt.verify(Token, secretKey, (err) => {
            if (err) {
                // console.log(err);
                next({ Error: 'Error in Token creation', msg: 'Token is not created.' })
            } else {
                // console.log("Ashish");
                next();
            }

        });
    } else {
        // console.log("FORBIDDEN");
        //FORBIDDEN
        next({ Error: 'Unauthorized', msg: 'You are not authorized to access this page.' });
    }
}

module.exports = {
    verifyToken
}