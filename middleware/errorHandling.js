const createError = require('http-errors');

// error Handler Middleware
function errorHandler(err, req, res, next) {
    if (err.Error === 'Server Error') {
        return next(createError(500, 'Database Error: ' + err.msg));
    }

    if (err.Error === 'Not Acceptable') {
        return next(createError(406, 'Not Acceptable', err.error));
    }

    if (err.Error === 'Request Not Found') {
        return next(createError(404, err.msg));
    }

    if (err.Error === 'Error in Token creation') {
        return next(createError(500, err.msg));
    }

    if (err.Error === 'Forbidden') {
        return next(createError(403, err.msg));
    }

    if (err.Error === 'Bad Request') {
        return next(createError(400, err.msg, err.err));
    }

    if (err.Error === 'Unauthorized') {
        return next(createError(401, 'unauthorized'));
    }

    return next();
}

module.exports = errorHandler;