const express = require('express');
const router = express.Router();
const queries = require('../app/model/queries');
const validation = require('../validators/schemaValidation');
const verify = require('../middleware/verifyToken');
const bycrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/*========================
Register New User
========================*/

router.post('/register', (req, res, next) => {
    const user = {
        UserName: req.body.username,
        email: req.body.email,
        password: bycrypt.hashSync(req.body.password)
    };

    queries.Queries(queries.allQuery.addingUser, user)
        .then((result) => {
            res.send({ msg: `User Created Successfully` });
        }).catch((err) => {
            next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' });
        });
});

/*=========================
Authentication
=========================*/

router.post('/login', (req, res) => {
    const userLogin = {
        email: req.body.email
    };

    queries.Queries(queries.allQuery.findUserByEmail, userLogin.email)
        .then((result) => {
            // console.log(result);
            const secretKey = "animeLover0606";
            let USER = {
                id: result[0].userId,
                userName: result[0].UserName,
                email: result[0].email,
                password: req.body.password
            }

            jwt.sign(USER, secretKey, { expiresIn: '2d' }, (err, token) => {
                res.send({ user: result[0], access_token: token });
            });
        })
        .catch((err) => {
            next({ Error: 'Server Error', msg: 'Something happened server crashed!!!!!' })
        });
});

module.exports = router;